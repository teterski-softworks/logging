package logging

import (
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/teterski-softworks/logging/v2/errortrace"
	"gitlab.com/teterski-softworks/logging/v2/message"
)

type Logger interface {
	SetDateFormat(string)
	Write(message.Messenger)
	Close() errortrace.ErrorTracer
}

type Log struct {
	logger     log.Logger
	file       *os.File
	dateFormat string
}

/*New creates a new Log.

filePath - The path of the error log file.
*/
func New(filePath string) (Logger, errortrace.ErrorTracer) {

	/*Returns a Logger (a pointer) due to log.Logger containing a sync.Mutex struct.
	  If just a Log is returned, the mutex will get copied. Returning a Logger ensures
	  a pointer to the mutex is returned.

	  Its not a big issue in this case, but the compiler can complain about it.
	*/

	var l Log
	var err error
	l.file, err = os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return &l, errortrace.New(err)
	}
	writer := io.MultiWriter(os.Stdout, l.file)
	l.logger.SetOutput(writer)
	l.SetDateFormat("Jan-02-2006 3:04:05 PM")
	return &l, nil
}

//SetDateFormat sets the date format used by the log.
func (l *Log) SetDateFormat(format string) {
	l.dateFormat = format
}

//Close closes the files used by the log.
func (l *Log) Close() errortrace.ErrorTracer {
	if err := l.file.Close(); err != nil {
		return errortrace.New(err)
	}
	return nil
}

//Write writes the ErrorTracer to a log file.
func (l *Log) Write(m message.Messenger) {

	//If nothing is passed in, we don't bother to write it to the logger.
	if m == nil {
		return
	}

	fatalTag := ""
	if m.Fatal() {
		fatalTag = fmt.Sprintf("[FATAL][CODE %d]", m.ExitCode())
	}

	header := fmt.Sprintf("[%s]%s", m.Time().Format(l.dateFormat), fatalTag)
	l.logger.Printf("%s%s", header, m.String())
	if m.Fatal() {
		l.Close() //This can cause an error, but due to forced program termination, it won't be handled.
		os.Exit(m.ExitCode())
	}
}
