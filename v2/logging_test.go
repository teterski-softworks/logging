package logging_test

import (
	"bufio"
	"fmt"
	"os"
	"testing"

	"gitlab.com/teterski-softworks/logging/v2"
	"gitlab.com/teterski-softworks/logging/v2/message"
	"gitlab.com/teterski-softworks/testinghelpers"
)

//!Test coverage can show as incomplete, but that is due to the testing tools
//!innability to see what testinghelpers.ExitCodeTest is doing.
func Test_Logger(t *testing.T) {
	testinghelpers.Implements(&logging.Log{}, (*logging.Logger)(nil), t)
	var _ logging.Logger = &logging.Log{}
}

func Test_New_Close(t *testing.T) {

	signature := fmt.Sprintf("\n%T.Test_New_Add_Length_getCaller - ", logging.Log{})
	l, err := logging.New("./testdata/test.log")

	if err != nil {
		t.Errorf("%s %s", signature, err.Error())
	}
	if err := l.Close(); err != nil {
		t.Errorf("%s %s", signature, err.Error())
	}

	l, err = logging.New("./invalid\\/path?!23ds_~-';\n")
	//Testing the errors
	if err == nil {
		t.Errorf("%s %s", signature, err.Error())
	}
	if err = l.Close(); err == nil {
		t.Errorf("%s %s", signature, err.Error())
	}

}

func Test_Write_SetDateFormat(t *testing.T) {

	signature := fmt.Sprintf("\n%T.Test_New_Add_Length_getCaller - ", logging.Log{})

	//Erase the file, to make sure we are starting with a blank file.
	os.Remove("./testdata/test.log")

	m := message.New("Test message")

	l, err := logging.New("./testdata/test.log")
	if err != nil {
		t.Fatalf("%s%s", signature, err.Error())
	}
	dateFormat := "2006-01-02"
	l.SetDateFormat(dateFormat)
	l.Write(m)
	l.Write(nil)

	if err := l.Close(); err != nil {
		t.Fatalf("%s%s", signature, err.Error())
	}

	file, err1 := os.Open("./testdata/test.log")

	if err1 != nil {
		t.Fatalf("%s%s", signature, err1.Error())
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var result []string
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}
	if err := file.Close(); err != nil {
		t.Fatalf("%s%s", signature, err.Error())
	}
	if len(result) != 1 {
		t.Fatalf("%sWrote more than one line.", signature)
	}

	expected := fmt.Sprintf("[%s]Test message", m.Time().Format(dateFormat))
	if result[0] != expected {
		t.Fatalf("%s\nExpected:\n%v\nActual:\n%v\n\n", signature, expected, result[0])
	}
}

//Tests writing a fatal message to the log.
func Test_Write_Fatal(t *testing.T) {

	signature := fmt.Sprintf("\n%T.Test_New_Add_Length_getCaller - ", logging.Log{})

	exitCode := 20
	//Erase the file, to make sure we are starting with a blank file.
	os.Remove("./testdata/test.log")

	m := message.New("Test message")
	m.SetFatal(true)
	m.SetExitCode(exitCode)
	var test func(t *testing.T) = func(t *testing.T) {

		l, err := logging.New("./testdata/test.log")
		if err != nil {
			t.Fatalf("%s%s", signature, err.Error())
		}

		l.Write(m)
		l.Write(nil)

		if err := l.Close(); err != nil {
			t.Fatalf("%s%s", signature, err.Error())
		}

	}

	testinghelpers.ExitCodeTest(test, "Test_Write_Fatal", t, exitCode)
	file, err1 := os.Open("./testdata/test.log")

	if err1 != nil {
		t.Fatalf("%s%s", signature, err1.Error())
	}
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var result []string
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}
	if err := file.Close(); err != nil {
		t.Fatalf("%s%s", signature, err.Error())
	}
	if len(result) != 1 {
		t.Fatalf("%sWrote more than one line.", signature)
	}

	expected := fmt.Sprintf("[%s][FATAL][CODE %d]Test message", m.Time().Format("Jan-02-2006 3:04:05 PM"), exitCode)
	if result[0] != expected {
		t.Fatalf("%s\nExpected:\n%v\nActual:\n%v\n\n", signature, expected, result[0])
	}
}
