package message

import (
	"fmt"
	"time"
)

//Messager is timestamped string, which when written, may or may not end the program.
type Messenger interface {
	String() string
	Time() time.Time
	Fatal() bool
	SetFatal(bool)
	ExitCode() int
	SetExitCode(int)
}

//Message is timestamped string, which when written, may or may not end the program.
type Message struct {
	string
	time     time.Time
	fatal    bool
	exitCode int
}

//New returns a new Message. Format is a string format specifier.
func New(format string, a ...interface{}) Messenger {

	var m Message
	m.SetFatal(false)
	m.string = fmt.Sprintf(format, a...)
	m.time = time.Now()

	return &m
}

//String returns teh Message's string value.
func (m Message) String() string {
	return m.string
}

//Time returns the timestamp of the message.
func (m Message) Time() time.Time {
	return m.time
}

//Fatal returns a true if the message should stop execution, or false if it is to continue running.
func (m Message) Fatal() bool {
	return m.fatal
}

//SetFatal if set to true code execution is to stop when the message is reached.
//If fatal is set to true, sets the exit code to 1, by default.
//If fatal is set to false, sets the exit code to 0.
func (m *Message) SetFatal(fatal bool) {
	m.fatal = fatal
	m.SetExitCode(0)
	if fatal {
		m.SetExitCode(1)
	}

}

//SetExitCode sets the message's exit code, which will be used if the message is fatal.
func (m *Message) SetExitCode(code int) {
	m.exitCode = code
}

//ExitCode gets the message's exit code.
func (m Message) ExitCode() int {
	return m.exitCode
}
