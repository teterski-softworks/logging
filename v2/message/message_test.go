package message_test

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/teterski-softworks/logging/v2/message"
	"gitlab.com/teterski-softworks/testinghelpers"
)

func Test_Messager(t *testing.T) {
	testinghelpers.Implements(&message.Message{}, (*message.Messenger)(nil), t)
	var _ message.Messenger = &message.Message{}
}

func Test_New_String_Time(t *testing.T) {

	m := message.New("Test %s %d %f", "test", 1, 3.14)
	signature := fmt.Sprintf("\n%T.Test_New_String_Time - ", m)

	if testinghelpers.TimeWithinThresholdFromNow(m.Time().Add(time.Millisecond), time.Millisecond) {
		t.Errorf("%sTime threshhold exceeded.", signature)
	}
	result := m.String()
	expected := fmt.Sprintf("Test %s %d %f", "test", 1, 3.14)
	if result != expected {
		t.Errorf("%sExpected message text: %s Actual: %s", signature, expected, result)
	}

	if m.Fatal() {
		t.Errorf("%sExpected fatal: %v Actual: %v", signature, false, m.Fatal())
	}

	if m.ExitCode() != 0 {
		t.Errorf("%sExpected exit code: %d Actual: %d", signature, 0, m.ExitCode())
	}

}

func Test_Fatal_SetFatal(t *testing.T) {

	m := message.New("")
	signature := fmt.Sprintf("\n%T.Test_Fatal_SetFatal - ", m)

	m.SetFatal(false)
	if m.Fatal() {
		t.Errorf("%sExpected: %v Actual: %v", signature, false, m.Fatal())
	}
	m.SetFatal(true)
	if !m.Fatal() {
		t.Errorf("%sExpected: %v Actual: %v", signature, true, m.Fatal())
	}
	exitCode := 1
	if m.ExitCode() != exitCode {
		t.Errorf("%sExpected exit code: %d Actual: %d", signature, exitCode, m.ExitCode())
	}

}

func Test_ExitCode_SetExitCode(t *testing.T) {
	var m message.Message
	signature := fmt.Sprintf("\n%T.Test_ExitCode_SetExitCode - ", m)

	exitCode := 2
	m.SetExitCode(exitCode)
	if m.ExitCode() != exitCode {
		t.Errorf("%sExpected: %d Actual: %d", signature, exitCode, m.ExitCode())
	}

}
