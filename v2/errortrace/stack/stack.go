package stack

import (
	"runtime"
	"strings"
)

/*
//Stacker contains an array of function calls and an array of their corresponding source code line numbers.
type Stacker interface {
	Add(string, string, int)
	Length() int
}*/

//Stack contains an array of function calls and an array of their corresponding source code line numbers.
type Stack struct {
	//An array that contains a stack of source files names.
	Files []string
	//An array containing a stack of function names.
	Functions []string
	//An array containing a stack of line numbers.
	Lines []int
}

//New gets the entire stack of the application
func New() Stack {

	var stack Stack

	//the loop stops printing the stack once it reaches the runtime.goexit function, which is the starting point of any Go stack.
	for offset, line, file, function := 0, 0, "", ""; function != "runtime.goexit"; offset++ {
		file, function, line = getCaller(offset)
		stack.Add(file, function, line)
	}
	return stack
}

//Length returns the length of the stack.
func (s Stack) Length() int {
	return len(s.Lines)
}

//Add adds a function and its source code line to the stack.
func (s *Stack) Add(file string, function string, line int) {
	s.Files = append(s.Files, file)
	s.Functions = append(s.Functions, function)
	s.Lines = append(s.Lines, line)
}

/*getCaller will return info about foo(), if foo has called bar(), and bar() has made the current call to getCaller()

Returns two values, the Title of the function (foo), and the line number from which foo() called bar().
*/
func getCaller(offset int) (fileName string, functionName string, line int) {
	pc := make([]uintptr, 10) // at least 1 entry needed

	//We need to start at 3, as we need to look at the functions 3+ places back on the stack to find the error function.
	//1 place back would be this function
	//2 places back would be the caller of this function (the getStack() function above)
	//3 places back is the grandcaller of this function (one of the error functions above, which call getStack())
	runtime.Callers(3+offset, pc)
	f := runtime.FuncForPC(pc[0])
	fileName, line = f.FileLine(pc[0])
	functionPath := strings.Split(f.Name(), "/")
	functionName = functionPath[len(functionPath)-1]

	return fileName, functionName, line
}
