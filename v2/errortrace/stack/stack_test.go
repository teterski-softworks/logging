package stack_test

import (
	"fmt"
	"testing"

	"gitlab.com/teterski-softworks/logging/v2/errortrace/stack"
	"gitlab.com/teterski-softworks/testinghelpers"
)

func Test_New_Add_Length_getCaller(t *testing.T) {
	result := stack.New()
	signature := fmt.Sprintf("\n%T.Test_New_Add_Length_getCaller - ", result)
	var expected stack.Stack
	expected.Add("/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/stack/stack_test.go", "stack_test.Test_New_Add_Length_getCaller", 12)
	expected.Add("/usr/local/go/src/testing/testing.go", "testing.tRunner", 1196)
	expected.Add("/usr/local/go/src/runtime/asm_amd64.s", "runtime.goexit", 1372)
	if expected.Length() != 3 {
		t.Errorf("%s Incorrect length.\nExpected: %v\nActual: %v", signature, 3, expected.Length())
	}
	if !testinghelpers.ValueEquals(result, expected) {
		t.Errorf("%s\nExpected %v\nActual %v", signature, expected, result)
	}
}
