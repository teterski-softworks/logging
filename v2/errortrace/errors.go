package errortrace

//ErrorNilPointer is an error thrown if an nil pointer is passed through.
func ErrorNilPointer() ErrorTracer {
	return Newf("nil pointer passed through")
}
