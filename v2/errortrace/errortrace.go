package errortrace

import (
	"fmt"

	"gitlab.com/teterski-softworks/logging/v2/errortrace/stack"
	"gitlab.com/teterski-softworks/logging/v2/message"
)

//ErrorTracer is an error that contains stack and identifier information allowing for easier debuging.
type ErrorTracer interface {
	error
	message.Messenger
	Stack() stack.Stack
}

//ErrorTrace is an error that contains stack and identifier information allowing for easier debuging.
type ErrorTrace struct {
	message.Messenger
	stack stack.Stack
}

//New takes a regular error and converts it to an ErrorTrace. If nil is passed in, nil is returned.
func New(e error) ErrorTracer {
	//We want to return a ErrorTracer, rather than an ErrorTrace, so that the returned object can be equal to nil,
	//just like a regular Golang error can.

	if e == nil {
		return nil
	}

	return Newf(e.Error())
}

//Newf returns an ErrorTracer and sets the formatable string as its text.
func Newf(format string, a ...interface{}) ErrorTracer {
	return NewFromMessenger(message.New(format, a...))
}

//NewFromMessenger converts a message.Messenger into an ErrorTracer.
func NewFromMessenger(m message.Messenger) ErrorTracer {
	var err ErrorTrace
	err.Messenger = m
	err.stack = stack.New()
	return &err
}

//Error returns the text of the ErrorTrace.
func (err ErrorTrace) Error() string {
	return err.Messenger.String()
}

//String returns the string representation of the error message and the Stack.
func (err ErrorTrace) String() string {

	text := err.Error()

	for i := 0; i < err.stack.Length(); i++ {
		text = fmt.Sprintf("%s\n%s:%d\t%s", text, err.stack.Files[i], err.stack.Lines[i], err.stack.Functions[i])
	}

	return text
}

//Stack returns the stack of the ErrorTrace.
func (err ErrorTrace) Stack() stack.Stack {
	return err.stack
}
