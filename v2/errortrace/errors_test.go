package errortrace_test

import (
	"testing"

	"gitlab.com/teterski-softworks/logging/v2/errortrace"
)

func Test_Errors_NilPointer(t *testing.T) {
	text := "nil pointer passed through"
	err := errortrace.ErrorNilPointer()

	if err.Fatal() {
		t.Errorf("\nNilPointer Fatal is incorrect.\nExpected: %v Actual: %v\n\n", false, err.Fatal())
	}
	if err.Error() != text {
		t.Errorf("\nNilPointer Text does not match.\nExpected: %v\nActual: %v\n\n", text, err.Error())
	}
}
