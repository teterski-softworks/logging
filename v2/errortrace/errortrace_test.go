package errortrace_test

import (
	"fmt"
	"reflect"
	"runtime"
	"testing"
	"time"

	"gitlab.com/teterski-softworks/logging/v2/errortrace"
	"gitlab.com/teterski-softworks/logging/v2/errortrace/stack"
	"gitlab.com/teterski-softworks/testinghelpers"
)

func Test_ErrorTracer(t *testing.T) {
	testinghelpers.Implements(&errortrace.ErrorTrace{}, (*errortrace.ErrorTracer)(nil), t)
	var _ errortrace.ErrorTracer = &errortrace.ErrorTrace{}
}

func Test_New_Newf_NewFromMessenger(t *testing.T) {

	var text = "This is a test error."

	/*This is needed to automate the stack line number
	for the error call.*/
	pc := make([]uintptr, 10)

	runtime.Callers(0, pc)
	f := runtime.FuncForPC(pc[0])
	_, line := f.FileLine(pc[0])

	var err = errortrace.New(fmt.Errorf(text))
	signature := fmt.Sprintf("\n%T.Test_New_Newf_NewFromMessenger - ", err)
	/*Comparing correct time can be difficult
	given that some time passes between when we take
	our initial measurement and the time it takes to
	generate the error.

	We want to allow a degree of error of 1 millisecond.
	Needless to say this can error out due to a variety of factors, but isn't a major concern.
	*/
	if testinghelpers.TimeWithinThresholdFromNow(err.Time(), time.Millisecond) {
		t.Errorf("%sTime threshhold exceeded.", signature)
	}

	var stack stack.Stack
	//Most of these lines can change position, so we have to pass in the same line position.
	stack.Add("/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace.go", "errortrace.NewFromMessenger", err.Stack().Lines[0])
	stack.Add("/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace.go", "errortrace.Newf", err.Stack().Lines[1])
	stack.Add("/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace.go", "errortrace.New", err.Stack().Lines[2])
	//We do however know where this line will be, because its in this file, so we can use it to test the line finding behavior.
	stack.Add("/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace_test.go", "errortrace_test.Test_New_Newf_NewFromMessenger", line+4)
	stack.Add("/usr/local/go/src/testing/testing.go", "testing.tRunner", err.Stack().Lines[4])
	stack.Add("/usr/local/go/src/runtime/asm_amd64.s", "runtime.goexit", err.Stack().Lines[5])
	if !reflect.DeepEqual(err.Stack(), stack) {
		t.Errorf("%sStack is incorrect. Expected:\n%v\nActual:\n%v", signature, stack, err.Stack())
	}

	if err.Fatal() {
		t.Errorf("%sFatal is incorrect.\nExpected: %v Actual: %v", signature, false, err.Fatal())
	}
	if err.Error() != text {
		t.Errorf("%sString does not match.\nExpected: %v\nActual: %v", signature, text, err.Error())
	}

	err = errortrace.New(nil)
	if err != nil {
		t.Errorf("%s should be nil.\nExpected: %v\nActual: %v", signature, nil, err)
	}

}

func Test_String_Error(t *testing.T) {

	err := errortrace.Newf("Test")
	signature := fmt.Sprintf("\n%T.Test_String_Error - ", err)
	text := err.String()
	expected := "Test\n" +
		"/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace.go:44	errortrace.NewFromMessenger\n" +
		"/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace.go:37	errortrace.Newf\n" +
		"/home/nik/go/src/gitlab.com/teterski-softworks/logging/v2/errortrace/errortrace_test.go:76	errortrace_test.Test_String_Error\n" +
		"/usr/local/go/src/testing/testing.go:1196	testing.tRunner\n" +
		"/usr/local/go/src/runtime/asm_amd64.s:1372	runtime.goexit"

	if text != expected {
		t.Errorf("%sExpected: %v\nActual: %v", signature, expected, text)
	}

}
